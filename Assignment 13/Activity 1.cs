using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//The code below is made to take a single line of string data, and separates it to different component
//these are first name and last name
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            string first;
            string last;
            bool data;
            do
            {
                name = GetName();
                data = Vald(name);
            } while (data == false);
            first = GetFirst(name);
            last = GetLast(name);
            Display(first, last);
        }

        private static string GetName()
        {
            string name;
            int index;
            do
            {
                Console.WriteLine("Enter your name (first last):");
                name = Console.ReadLine();
                index = name.IndexOf(" ");
                if (index == -1 )
                {
                    Console.WriteLine("Please use a space or last name . . .\n");
                }
            } while (index < 0);
            return name;
        }

        private static bool Vald(string name)
        {
            int countSpaces;
            bool whitenull;
            bool letters;
            int index;
            string spaceScheck = name.Substring(0);
            letters = name.All(c => Char.IsLetter(c) || c == ' ');
            index = name.IndexOf(" ");
            if(letters == false)
            {
                Console.WriteLine("Please only use letters . . .\n");
                return false;
            }
            if (spaceScheck == " ")
            {
                Console.WriteLine("Please do not use a space at the start");
                return false;
            }
            if (index < 0)
            {
                Console.WriteLine("Please enter a space between your first and last name . . .\n");
                return false;
            }
            countSpaces = Spaces(name);
            if (countSpaces > 1)
            {
                Console.WriteLine("Please don't use more than one space . . .\n");
                return false;
            }
            whitenull = IsNullorWhitespace(name, index);
            if (whitenull == true)
            {
                Console.WriteLine("You have not given a last name . . .\n");
                return false;
            }
            if (index == 0 )
            {
                Console.WriteLine("You did not enter a first name or you have entered a space at the start . . .\n");
                return false;
            }
            return true;
        }

        public static bool IsNullorWhitespace(string name, int index)
        {
            if (name == null)
            {
                return true;
            }

            for (int i = index; i < name.Length; i++)
            {
                if (!Char.IsWhiteSpace(name[i]))
                {
                    return false;
                }
            }
            for (int i = 0; i < name.Length - index - 1; i ++ )
            {
                if (!Char.IsWhiteSpace(name[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private static int Spaces(string name)
        {
            int countSpaces = name.Count(Char.IsWhiteSpace);
            return countSpaces;
        }

        private static string GetFirst(string name)
        {
            string first;
            first = name.Substring(0,1);
            first = char.ToUpper(first[0]) + first.Substring(1);
            return first;
        }

        private static string GetLast(string name)
        {
            string last;
            int index;

            index = name.IndexOf(" ");
            if (index < 0)
            {
                last = "";
            }
            else
            {
                last = name.Substring(index + 1, name.Length - index - 1);
                last = last.Trim();
                last = char.ToUpper(last[0]) + last.Substring(1);
            }
            return last;
        }

        private static void Display(string first, string last)
        {
            Console.WriteLine("Hello " + last + ", " + first + ".");
        }
    }
}