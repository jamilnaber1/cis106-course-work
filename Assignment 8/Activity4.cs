using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            UsOrMetric();
        }

        private static void UsOrMetric()
        {
            double miles;
            miles = GetMiles();
            string input;
            Console.WriteLine("Would you like this in US measurements or metric measurements? ");
            input = Console.ReadLine();
            if (input == "US" || input == "us")
            {

                ReadYards(miles);
                ReadFeet(miles);
                ReadInchs(miles);
            }
            else if (input == "Metric" || input == "metric")
            {
                ReadKilo(miles);
                ReadMeter(miles);
                Readcenti(miles);
            }
            else
            {
                Console.WriteLine("Please enrter a vaild input, US for US measurements, and Metric for metric measurements.");
            }
        }

        private static double GetMiles()
        {
            string input;
            double miles;
            Console.WriteLine("Please enter your mile amount: ");
            input = Console.ReadLine();
            miles = Convert.ToDouble(input);

            return miles;
        }

        private static void ReadYards(double miles)
        {
            string distance = "yards";
            miles = CalYards(miles);
            Display(miles, distance);
        }

        private static void ReadFeet(double miles)
        {
            string distance = "feet";
            miles = CalFeet(miles);
            Display(miles, distance);
        }

        private static void ReadInchs(double miles)
        {
            string distance = "inchs";
            miles= CalInchs(miles);
            Display(miles, distance);
        }

        private static void ReadKilo(double miles)
        {
            string distance = "kilo";
            miles = CalKilo(miles) ;
            Display(miles, distance);
        }

        private static void ReadMeter(double miles)
        {
            string distance = "meters";
            miles = CalMeters(miles);
            Display(miles, distance);
        }

        private static void Readcenti(double miles)
        {
            string distance = "centimeters";
            miles = CalCentimeters(miles);
            Display(miles, distance);
        }

        private static double CalYards(double miles)
        {
            miles = miles * 1760;
            return miles;
        }

        private static double CalFeet(double miles)
        {
            miles = miles * 1760 * 3;
            return miles;
        }

        private static double CalInchs(double miles)
        {
            miles = miles * 1760 * 3 * 12;
            return miles;
        }

        private static double CalKilo(double miles)
        {
            miles = miles * 1.609344;
            return miles;
        }

        private static double CalMeters(double miles)
        {
            miles = miles * 1609.34;
            return miles;
        }

        private static double CalCentimeters(double miles)
        {
            miles = miles * 160934;
            return miles;
        }

        private static void Display(double miles, string distance)
        {
            Console.WriteLine("This is the amount of " + distance + " " + miles);
        }
   }
}