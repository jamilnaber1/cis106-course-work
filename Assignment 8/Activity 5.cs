using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    //This program uses the users age, in years, to calculate their age in other time units
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("checkpoint");
            IfElse();
        }

        private static void IfElse()
        {
            string input;
            Console.WriteLine("Please enter the shape you want to find the area of. . .");
            input = Console.ReadLine();
            if (input == "Circle" || input == "C")
            {
                GotoCircle();
            }
            else if (input == "Parallelogram" || input == "P")
            {
                GotoParallelogram();
            }
            else if (input == "Rectangle" || input == "R")
            {
                GotoRectangle();
            }
            else if (input == "Square" || input == "S")
            {
                GotoSquare();
            }
            else if (input == "Trapezoid" || input == "TRA")
            {
                GotoTrapezoid();
            }
            else if (input == "Triangle" || input == "TRI")
            {
                GotoTriangle();
            }
            else
            {
                Console.WriteLine("Please enter a vaild value . . . ");
                return;
            }
        }

        private static void GotoCircle()
        {
            string type = "circle";
            double radius;
            double Area;

            radius = GetRadius();
            Area = CalCricle(radius);
            Display(Area, type);
        }

        private static void GotoParallelogram()
        {
            string type = "parallelogram";
            double Base;
            double Height;
            double Area;

            Base = GetBase();
            Height = GetHeight();
            Area = CalParallelogram(Base, Height);
            Display(Area, type);
        }

        private static void GotoRectangle()
        {
            string type = "Rectangle";
            double Height;
            double Width;
            double Area;

            Height = GetHeight();
            Width = GetWidth();
            Area = CalRectangle(Height, Width);
            Display(Area, type);
        }

        private static void GotoSquare()
        {
            string type = "square";
            double Length;
            double Area;

            Length = GetLength();
            Area = CalSquare(Length);
            Display(Area, type);
        }

        private static void GotoTrapezoid()
        {
            string type = "trapezoid";
            double Top;
            double Bottom;
            double Height;
            double Area;

            Top = GetTop();
            Bottom = GetBottom();
            Height = GetHeight();
            Area = CalTrapezoid(Top, Bottom, Height);
            Display(Area, type);
        }

        private static void GotoTriangle()
        {
            string type = "triangle";
            double Base;
            double Height;
            double Area;

            Base = GetBase();
            Height = GetHeight();
            Area = CalTriangle(Base, Height);
            Display(Area, type);
        }

        private static double GetRadius()
        {
            string input;
            double radius;
            Console.WriteLine("Please enter the radius");
            input = Console.ReadLine();
            radius = Convert.ToDouble(input);
            return radius;
        }

        private static double GetBase()
        {
            string input;
            double Base;
            Console.WriteLine("Please enter the base");
            input = Console.ReadLine();
            Base = Convert.ToDouble(input);
            return Base;
        }

        private static double GetHeight()
        {
            string input;
            double Height;
            Console.WriteLine("Please enter the height");
            input = Console.ReadLine();
            Height = Convert.ToDouble(input);
            return Height;
        }

        private static double GetWidth()
        {
            string input;
            double Width;
            Console.WriteLine("Please enter the width");
            input = Console.ReadLine();
            Width = Convert.ToDouble(input);
            return Width;
        }

        private static double GetLength()
        {
            string input;
            double Length;
            Console.WriteLine("Please enter the length");
            input = Console.ReadLine();
            Length = Convert.ToDouble(input);
            return Length;
        }

        private static double GetTop()
        {
            string input;
            double Top;
            Console.WriteLine("Please enter the top");
            input = Console.ReadLine();
            Top = Convert.ToDouble(input);
            return Top;
        }

        private static double GetBottom()
        {
            string input;
            double Bottom;
            Console.WriteLine("Please enter the bottom");
            input = Console.ReadLine();
            Bottom = Convert.ToDouble(input);
            return Bottom;
        }

        private static double CalCricle(double radius)
        {
            double Area;
            Area = radius * radius * 3.1415926;
            return Area;
        }

        private static double CalParallelogram(double Base, double Height)
        {
            double Area;
            Area = Height * Base;
            return Area;
        }

        private static double CalRectangle(double Height, double Width)
        {
            double Area;
            Area = Width * Height;
            return Area;
        }

        private static double CalSquare(double Length)
        {
            double Area;
            Area = Length * Length;
            return Area;
        }

        private static double CalTrapezoid(double Top, double Bottom, double Height)
        {
            double Area;
            Area = ((Top + Bottom) / 2 * Height);
            return Area;
        }

        private static double CalTriangle(double Base, double Height)
        {
            double Area;
            Area = Base * Height / 2;
            return Area;
        }

        private static void Display(double Area, string type)
        {
            Console.WriteLine("The area of the " + type + " is " + Area);
        }
    }
}
