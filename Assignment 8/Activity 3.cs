using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    //This program uses the users age, in years, to calculate their age in other time units
    class Program
    {
        static void Main(string[] args)
        {
            double Age;
            Age = GetAge();
            Timeframe(Age);
        }

        private static double GetAge()
        {
            string input;
            double Age;
            Console.WriteLine("How old are you in years? ");
            input = Console.ReadLine();
            Age = Convert.ToDouble(input);

            return Age;
        }

        public static void Timeframe(double Age)
        {
            string input;
            Console.WriteLine("Pick a timeframe of Months, Days, Hours, Seconds");
            input = Console.ReadLine();
            if (input == "Months" || input == "M")
            {
                ReadMonths(Age);
            }
            else if (input == "Days" || input == "D")
            {
                ReadDays(Age);
            }
            else if (input == "Hours" || input == "H")
            {
                ReadHours(Age);
            }
            else if (input == "Seaconds" || input == "S")
            {
                ReadSeconds(Age);
            }
            else
            {
                Console.WriteLine("Please enter a vaild input (M,D,H,S)");
                Timeframe(Age);
            }
        }

        private static void ReadMonths(double Age)
        {
            string time = "Months"; 
            Age= CalMonths(Age);
            Outputline(Age, time);
        }

        private static void ReadDays(double Age)
        {
            string time = "Days";
            Age = CalDays(Age);
            Outputline(Age, time);
        }

        private static void ReadHours(double Age)
        {
            string time = "Hours";
            Age = CalHours(Age);
            Outputline(Age, time);

        }

        private static void ReadSeconds(double Age)
        {
            string time = "Seconds";
            Age = CalSeconds(Age);
            Outputline(Age, time);
        }

        private static double CalMonths(double Age)
        {
            Age = Age * 12;
            return Age;
        }

        private static double CalDays(double Age)
        {
            Age = Age * 365;
            return Age;
        }

        private static double CalHours(double Age)
        {
            Age = Age * 365 * 24;
            return Age;
        }

        private static double CalSeconds(double Age)
        {
            Age = Age * 365 * 24 * 60 * 60;
            return Age;
        }

        private static void Outputline(double Age, string time)
        {
            Console.WriteLine("This Is Your Age In " + time + " is " + Age);
       }
    }
}