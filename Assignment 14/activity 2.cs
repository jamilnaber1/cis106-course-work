using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//The code takes a txt file found in the user computer, and uses it to find information related to scores
//they're three different outputs, High, Low, and Average.
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"C: \Users\Jamil's PC\source\repos\ConsoleApp8\TextFile1.txt";
            double highscore, lowscore, total, average;
            List<double> scoreh = new List<double>();
            if (System.IO.File.Exists(filename))
            {
                System.Console.WriteLine("File already exists.\n");
            }
            else
            {
                System.Console.WriteLine("File does not exist.\n");
            }
            scoreh = ReadFile(filename);
            highscore = High(scoreh);
            lowscore = Low(scoreh);
            total = TotalC(scoreh);
            average = AverageC(total, scoreh);
            DisplayR(highscore, lowscore, average);
        }

        private static List <double> ReadFile(string filename)
        {
            string line, stringn;
            double score;
            int index, counter;
            counter = 0;
            System.IO.StreamReader file;
            file = System.IO.File.OpenText(filename);
            List<double> scoreh = new List<double>();
            while ((line = file.ReadLine()) != null)
            {
                index = line.IndexOf(":");
                stringn = line.Substring(index + 1);
                score = Convert.ToDouble(stringn);
                scoreh.Add(score);
                Console.WriteLine("Array is stored as " + scoreh[counter] + "\n");
                counter++;
            }
            return scoreh;
        }

        private static double High(List<double> scoreh)
        {
            double highscore;
            highscore = scoreh.Max();
            return highscore;
        }

        private static double Low(List<double> scoreh)
        {
            double lowscore;
            lowscore = scoreh.Min();
            return lowscore;
        }

        private static double TotalC(List<double> scoreh)
        {
            double total;
            total = scoreh.Sum();
            return total;

        }

        private static double AverageC(double total, List <double> scoreh)
        {
            double average = total / scoreh.Count;
            return average;
        }

        private static void DisplayR(double highscore, double lowscore, double average)
        {
            Console.WriteLine("The high score was " + highscore);
            Console.WriteLine("The low score was " + lowscore);
            Console.WriteLine("The average score was " + average);
        }
    }
}