using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename;
            double max, min, average;
            List<double> scoreh = new List<double>();
            filename = @"C: \Users\Jamil's PC\source\repos\ConsoleApp8\TextFile1.txt";
            FileVald(filename);
            FileEmpty(filename);
            scoreh = ReadFile(filename);
            max = High(scoreh);
            min = Low(scoreh);
            average = Average(scoreh);
            DisplayR(max, min, average);
        }

        private static void FileVald(string filename)
        {
            try
            {
                if (!File.Exists(filename))
                {
                    throw new FileNotFoundException();
                }
                else
                {
                    Console.WriteLine("File Found");
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File Not Found");
            }
        }

        private static void FileEmpty(string filename)
        {
            try
            {
                if (new FileInfo(filename).Length == 0)
                {
                    throw new Exception();
                }
                else
                {
                    Console.WriteLine("File is not empty\n");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file is empty . . .\n");
            }
        }

        private static List<double> ReadFile(string filename)
        {
            bool ValdChecker;
            string line, stringn;
            int index, counter;
            double score;
            counter = 0;
            System.IO.StreamReader file;
            file = System.IO.File.OpenText(filename);
            List<double> scoreh = new List<double>();
            while ((line = file.ReadLine()) != null)
            {
                ValdChecker = LineInfoVald(line);
                if(ValdChecker == true)
                {
                    index = line.IndexOf(":");
                    stringn = line.Substring(index + 1);
                    score = Convert.ToDouble(stringn);
                    scoreh.Add(score);
                    Console.WriteLine("Array is stored as " + scoreh[counter] + "\n");
                    counter++;
                }               
            }
            return scoreh;
        }
        
        private static bool LineInfoVald(string line)
        {
            int index1 = line.IndexOf(":");
            int index2 = line.IndexOf(" ");
            int countSpaces;
            countSpaces = Spaces(line);
            try
            {
                if (index1 < 0 )
                {
                    Console.WriteLine("The line does not include a ':' . . .");
                    throw new Exception();
                }
                if (index1 < index2 )
                {
                    Console.WriteLine("The line does not have a space between the first or last name . . . ");
                    throw new Exception();
                }
                if (countSpaces > 2)
                {
                    Console.WriteLine("The line Has too many spaces . . . ");
                    throw new Exception();
                }
                if(Char.IsWhiteSpace(line[index1 + 2]))
                {
                    Console.WriteLine("The line does not contain a score . . . ");
                    throw new Exception();
                }
                return true;

            }

            catch (Exception e)
            {
                Console.WriteLine("This line will be skipped due to an error\n");
                return false;
            }



        }

        private static int Spaces(string line)
        {
            int countSpaces = line.Count(Char.IsWhiteSpace);
            return countSpaces;
        }

        private static double High( List<double> scoreh)
        {
            double max;
            max = scoreh.Max();
            return max;
        }

        private static double Low(List<double> scoreh)
        {
            double min;
            min = scoreh.Min();
            return min;
        }

        private static double Average(List<double> scoreh)
        {
            double average;
            average = scoreh.Sum()/ scoreh.Count;
            return average;
        }

        private static void DisplayR(double max, double min, double average)
        {
            Console.WriteLine("The high score was " + max);
            Console.WriteLine("The low score was " + min);
            Console.WriteLine("The average score was " + average);
        }
    }
}