using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//The code takes a txt file found in the user computer, and uses it to find information related to scores
//they're three different outputs, High, Low, and Average.
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"C: \Users\Jamil's PC\source\repos\ConsoleApp8\TextFile1.txt";
            ReadFile(filename);
            if (System.IO.File.Exists(filename))
            {
                System.Console.WriteLine("File already exists.\n");
            }
            else
            {
                System.Console.WriteLine("File does not exist.\n");
            }
        }

        private static void ReadFile(string filename)
        {
            string line;
            string stringn;
            int score, index, lenght, counter;
            double highscore, lowscore, total, average;
            highscore = 0;
            lowscore = 0;
            total = 0;
            counter = 0;
            average = 0;
            System.IO.StreamReader file;
            file = System.IO.File.OpenText(filename);
            while ((line = file.ReadLine()) != null)
            {
                index = line.IndexOf(":");
                lenght = line.Length;
                stringn = line.Substring(index + 1);
                score = Convert.ToInt32(stringn);
                highscore = High(score, highscore);
                lowscore = Low(score, lowscore);
                total = TotalC(score, total);
                counter++;
            }
            average = AverageC(total, counter);
            DisplayR(highscore, lowscore, average);
            file.Close();
            Console.WriteLine("");
        }

        private static double High(int score, double highscore)
        {
            if (highscore == 0)
            {
                highscore = score;
                return highscore;
            }
            else if (highscore <= score )
            {
                highscore = score;
                return highscore;
            }
            else
            {
                return highscore;
            }
        }

        private static double Low(int score, double lowscore)
        {
            if (lowscore == 0)
            {
                lowscore = score;
                return lowscore;
            }
            else if (lowscore >= score)
            {
                lowscore = score;
                return score;
            }
            else
            {
                return lowscore;
            }
        }

        private static double TotalC(int score, double total)
        {
            total = score + total;
            return total;
           
        }

        private static double AverageC(double total, double counter)
        {
            double average = total / counter;
            return average;
        }

        private static void DisplayR(double highscore, double lowscore, double average)
        {
            Console.WriteLine("The high score was " + highscore);
            Console.WriteLine("The low score was " +lowscore);
            Console.WriteLine("The average score was " + average);
        }
    }
}