using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        //This program uses functions to help it find and calculate date when it is asked to run. The main goal is to see the gross pay
        static void Main(string[] args)
        {
            double hours;
            double rateperhour;
            double gross;

            hours = GetHours();
            rateperhour = GetRateperhour();
            gross = CalculateGross(hours, rateperhour);
            DisplayResult(gross);

        }

        private static double GetHours()
        {
            string input;
            double hours;

            Console.WriteLine("How Many Hours Have You Worked ");
            input = Console.ReadLine();
            hours = Convert.ToDouble(input);
            return hours;
        }
        private static double GetRateperhour()
        {
            string input;
            double rateperhour;

            Console.WriteLine("What is your Pay Rate");
            input = Console.ReadLine();
            rateperhour = Convert.ToDouble(input);

            return rateperhour;
        }
        private static double CalculateGross(double hours, double rateperhour)
        {
            double gross;

            gross = hours * rateperhour;

            return gross;
        }
        private static void DisplayResult(double gross)
        {
            Console.WriteLine("This Is the Gross Amount Paid in Dollars: " + gross);
        }
    }

}