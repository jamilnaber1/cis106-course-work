using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
//This program uses the users age, in years, to calculate their age in other time units
    class Program
    {
        static void Main(string[] args)
        {
            double Age;
            Age = GetAge();
            ReadMonths(Age);
            ReadDays(Age);
            ReadHours(Age);
            ReadSeconds(Age);

        }

        private static double GetAge()
        {
            string input;
            double Age;
            Console.WriteLine("How old are you in years? ");
            input = Console.ReadLine();
            Age = Convert.ToDouble(input);

            return Age;

        }
        private static void ReadMonths(double Age)
        {
            double Month;
            Month = Age * 12;
            Console.WriteLine("This Is Your Age In Months: " + Month);
        }
        private static void ReadDays(double Age) 
        {
            double Days;
            Days = Age * 365;
            Console.WriteLine("This Is Your Age In Days: " + Days);
        }
        private static void ReadHours(double Age)
        {
            double Hours;
            Hours = Age * 365 * 24;
            Console.WriteLine("This Is Your Age In Hours: " + Hours);
           
        }
        private static void ReadSeconds(double Age)
        {
            double Seconds;
            Seconds = Age * 365 * 24 * 60 * 60;
            Console.WriteLine("This Is Your Age In Seconds: " + Seconds);
        }
    }

}
