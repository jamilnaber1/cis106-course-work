using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            double miles;
            miles = GetMiles();
            ReadYards(miles);
            ReadFeet(miles);
            ReadInchs(miles);

        }

        private static double GetMiles()
        {
            string input;
            double miles;
            Console.WriteLine("Please enter your mile amount: ");
            input = Console.ReadLine();
            miles = Convert.ToDouble(input);

            return miles;

        }
        private static void ReadYards(double miles)
        {
            double yards;
            yards = miles * 1760;
            Console.WriteLine("This is the amount of yards: "+ yards);
        }
        private static void ReadFeet(double miles) 
        {
            double feet;
            feet = miles * 1760 * 3;
            Console.WriteLine("This is the amount of Feets: " + feet);
        }
        private static void ReadInchs(double miles)
        {
            double inchs;
            inchs = miles * 1760 * 3 * 12;
            Console.WriteLine("This is the amount of Inchs: " + inchs);
           
        }

    }

}
