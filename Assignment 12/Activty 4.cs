using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This program is to show the proof of the Monty Hall problem
// We first do a sim of 100 times just randomly picking a door out of three
// Then we 
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] corg = new string[2];
            int[] door = new int[3];
            int pdoor;
            double counter = 0;
            Random rnd = new Random();
            for (int i = 0; i <= 100; i++)
            {
                corg = Setup();
                door = Doors(rnd);
                pdoor = DoorSelection(rnd);
                counter = ResultOne(pdoor, door, counter);
            }
            DisplayResult(counter);
            counter = 0;
            for (int i = 0; i <= 100; i++)
            {
                door = Doors(rnd);
                pdoor = DoorSelection(rnd);
                pdoor = OpenDoor(door, pdoor);
                counter = ResultOne(pdoor, door, counter);
            }
            DisplayResulttwo(counter);
        }

        private static string[] Setup()
        {
            string[] corg = new string[2];
            {
                corg[0] = "Goat";
                corg[1] = "Car";
            }

            return corg;
        }

        private static int[] Doors(Random rnd)
        {
            int[] door = new int[3];
            door[0] = rnd.Next(2);
            if (door[0] == 1)
            {
                door[1] = 0;
                door[2] = 0;
                return door;
            }
            else if (door[0] == 0)
            {
                door[1] = rnd.Next(2);
                if (door[1] == 1)
                {
                    door[2] = 0;
                    return door;
                }
                else if (door[1] == 0)
                {
                    door[2] = 1;
                }
                return door;
            }
            return door;
        }

        private static int DoorSelection(Random rnd)
        {
            int pdoor;
            pdoor = rnd.Next(3);
            return pdoor;
        }

        private static double ResultOne(int pdoor, int[] door, double counter)
        {
            if (door[pdoor] == 1)
            {
                counter++ ;
                return counter;
            }
            else
                return counter;
        }

        private static void DisplayResult(double counter)
        {
            double winavg = counter;
            Console.WriteLine("Your winning average without knowing one of the doors and not switching is a " + winavg + " % Chance \n");
        }

        private static int OpenDoor(int[] door, int pdoor)
        {
            if (door[0] == 0 && pdoor != 0)
            {
                if (pdoor == 1)
                {
                    pdoor = 2;
                    return pdoor;
                }
                else
                {
                    pdoor = 1;
                    return pdoor;
                }
            }
            
            else if (door[1] == 0 && pdoor != 1)
            {
                if (pdoor == 0)
                {
                    pdoor = 2;
                    return pdoor;
                }
                else
                { 
                    pdoor = 0;
                    return pdoor;
                }
            }
            else
            {
                if (pdoor == 0)
                {
                    pdoor = 1;
                    return pdoor;
                }
                else
                {
                    pdoor = 0;
                    return pdoor;
                }
            }
        }
        private static void DisplayResulttwo(double counter)
        {
            double winavg = counter;
            Console.WriteLine("Your winning average with knowing one of the doors and switching is a " + winavg + " % Chance \n");
        }
    }
}