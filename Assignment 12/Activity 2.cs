using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This program is to take in user input of a year, and checks if that year is a leap year.
// We Continue by asking the user which month they would like to know the day count of.
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int year;
            int month;
            int leapyear;
            year = UserData();
            leapyear = LeapYearCal(year);
            do
            {
                month = MonthInput();
                MonthOutput(month, leapyear);
            } while (1 <= month || month >= 12);
        }
        private static int UserData()
        {
            int year;
            Console.WriteLine("Please enter a year... ");
            year = Convert.ToInt32(Console.ReadLine());
            return year;
        }

        private static int LeapYearCal(int year)
        {
            int leapyear;
            if (year % 4 == 0)
            {
                if (year % 100 == 0)
                {
                    if (year % 400 == 0)
                    {
                        Console.WriteLine("\n" + year + " is a leap year\n");
                        leapyear = 1;
                        return leapyear;
                    }
                    else
                    {
                        Console.WriteLine("\n" + year + " is not a leap year\n");
                        leapyear = 0;
                        return leapyear;
                    }
                }
                else
                {
                    Console.WriteLine("\n" + year + " is a leap year\n");
                    leapyear = 1;
                    return leapyear;
                }
            }
            else
            {
                Console.WriteLine("\n" + year + " is not a leap year\n");
                leapyear = 0;
                return leapyear;
            }
        }

        private static int MonthInput()
        {
            int month;


            Console.WriteLine("\nPlease enter a month you wish to know the day count of with its corresponding number...");
            month = Convert.ToInt32(Console.ReadLine());
            return month;
        }

        private static int MonthOutput(int month, int leapyear)
        {
            int[] days = new int[13];
            {
                days[1] = 31;
                days[3] = 31;
                days[4] = 30;
                days[5] = 31;
                days[6] = 30;
                days[7] = 31;
                days[8] = 31;
                days[9] = 30;
                days[10] = 31;
                days[11] = 30;
                days[12] = 31;
            }
            if (leapyear == 1)
            {
                days[2] = 29;
            }
            else
            {
                days[2] = 28;
            }
            string[] mname = new string[13];
            {
                mname[1] = "January";
                mname[2] = "February";
                mname[3] = "March";
                mname[4] = "April";
                mname[5] = "May";
                mname[6] = "June";
                mname[7] = "July";
                mname[8] = "August";
                mname[9] = "September";
                mname[10] = "October";
                mname[11] = "November";
                mname[12] = "December";

            }
            Console.WriteLine("\n" + days[month] + " Is the amount of days in " + mname[month]);
            return month;
        }
    }
}