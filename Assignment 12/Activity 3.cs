using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This program is to take in user input of a user birhday.
// We use this data to find the day at which the date falls on.
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int year, month, day, fday;
            year = UserDataY();
            month = UserDataM();
            day = UserDataD();
            CheckYear(year, month);
            fday = DayCal(year, month, day);
            DisplayDay(fday);
        }

        private static int UserDataY()
        {
            int year;
            Console.WriteLine("Please enter your birth year ");
            year = Convert.ToInt32(Console.ReadLine());
            return year;
        }

        private static int UserDataM()
        {
            int month;
            Console.WriteLine("Please enter your birth month ");
            month = Convert.ToInt32(Console.ReadLine());
            if (month < 3)
            {
                month += 12;
                return month;
            }
            else
                return month;
        }

        private static int UserDataD()
        {
            int day;
            Console.WriteLine("Please enter your birth day ");
            day = Convert.ToInt32(Console.ReadLine());
            return day;
        }

        private static int CheckYear(int year, int month)
        {
            if (month > 12)
            {
                year -= 1;
                return year;
            }
            else
                return year;
        }
        private static int DayCal(int year, int month, int day)
        {
            int k = year % 100;
            int j = year / 100;
            int fday;
            fday = ((day + (13 * (month + 1) / 5) + k + (k / 4) + (j / 4) - (2 * j)) % 7);
            return fday;
        }

        private static void DisplayDay(int fday)
        {
            string[] weekdays = new string[7] { "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
            Console.WriteLine("The day was " + weekdays[fday] + ".");
        }
    }
}