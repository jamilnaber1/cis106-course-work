using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This program is to take in user input of scores, and the amount of scores.
// We then use this data to find the average of those scores 
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            int amount;
            double average;
            amount = UserInput();
            double[] score = new double [amount];
            ScoreCounter(amount, score);
            average = AvgScore(score, amount);
            Outputs(average, score);
        }

        private static int UserInput()
        {
            string input;
            int amount;
            Console.WriteLine("Please enter the amount of scores you would like to enter");
            input = Console.ReadLine();
            amount = Convert.ToInt32(input);
            return amount;
        }

        public static double[] ScoreCounter(int amount, double[] score)
        {
            int i;
            for (i = 0; i < amount; i++)
            {
                Console.WriteLine("Please enter a score in: ");
                score[i] = Convert.ToDouble(Console.ReadLine());
            }
            return score;
        }

        private static double AvgScore(double[] score, int amount)
        {
            double average;
            average = score.Sum() / amount;
            return average;
        }

        private static void Outputs(double average, double[] score)
        {
            double max = score.Max();
            double min = score.Min();
            Console.WriteLine("\nYour average is: " + average);
            Console.WriteLine("Your max is: " + max);
            Console.WriteLine("Your min is: " + min);

        }
    }
}