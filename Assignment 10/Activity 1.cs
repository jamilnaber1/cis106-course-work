using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This program is to take in user input of scores, and the amount of scores.
// We then use this data to find the average of those scores 
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            double amount;
            double score;
            amount = UserInput();
            score = ScoreCounter(amount);
            AvgScore(score, amount);
        }

        private static double UserInput()
        {
            string input;
            double amount;
            Console.WriteLine("Please enter the amount of scores you would like to enter");
            input = Console.ReadLine();
            amount = Convert.ToDouble(input);
            return amount;
        }

        private static double ScoreCounter(double amount)
        {
            int i = 1;
            double score = 0;
            double newscore;
            string loop;

            Console.WriteLine("Please pick while or for loop");
            loop = Console.ReadLine();
            if (loop == "While" || loop == "W")
            {
                while (i <= amount)
                {
                    string input;
                    Console.WriteLine("Please enter a score in: ");
                    input = Console.ReadLine();
                    newscore = Convert.ToDouble(input);
                    score = newscore + score;
                    i++;
                }
            }
            else if (loop == "For" || loop == "F")
            {
                for (i = 1; i <= amount; i++)
                {
                    string input;
                    Console.WriteLine("Please enter a score in: ");
                    input = Console.ReadLine();
                    newscore = Convert.ToDouble(input);
                    score = newscore + score;

                }
            }
            else
                Console.WriteLine("Please enter a vaild entry(W/F): ");
            return score;
        }

        private static void AvgScore(double score, double amount)
        {
            double average;
            average = score / amount;
            Console.WriteLine("Your average is: " + average);
        }
    }
}