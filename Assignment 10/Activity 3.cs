using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// This code takes in a start number and an end number to make a multiplication chart 
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
           int snum;
           int lnum;
           snum = Snum();
           lnum = Lnum();
           LoopTable(snum , lnum);      
        }

        private static int Snum()
        {
            string input;
            int snum;
            Console.WriteLine("Please enter your start number");
            input = Console.ReadLine();
            snum = Convert.ToInt16(input);
            return snum;
        }
        private static int Lnum()
        {
            string input;
            int lnum;
            Console.WriteLine("Please enter your last number");
            input = Console.ReadLine();
            lnum = Convert.ToInt16(input);
            return lnum;
        }

        private static void LoopTable (int snum, int lnum )
        {
            for (int i = snum - 1; i <= lnum; i++)
            {
                if (i == snum - 1) Console.Write("\t");
                else Console.Write(i + "\t");
                for (int j = snum; j <= lnum; j++)
                {
                    if (i > snum) Console.Write(i * j + "\t");
                    else Console.Write(j + "\t");
                }
                Console.Write("\n");
            }
        }
    }
}