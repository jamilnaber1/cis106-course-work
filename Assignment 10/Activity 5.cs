using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// In this code we are asking the user to pick a number form 0 - 100, and we need to be able to guess their number.
// We keep asking with a system of lower or higher where the change of number gets smaller (25-13-7-.....)
namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            LoopIfElse();
        }

        private static void LoopIfElse()
        {
            int number = 50;
            int numchange = 48;
            int counter = 1;
                string input;
                Console.WriteLine("Is your number " + number + " ?");
                input = Console.ReadLine();
            if (input == "Yes" || input == "yes" || input == "Y")
            {
                Console.WriteLine("I have geussed your number!");
                Console.WriteLine("It took me " + counter + " try");
            }
            else if (input == "No" || input == "no" || input == "N")
            {
                Boolean i;
                do
                {
                    Console.WriteLine("Is your number " + number +" higher, lower, or equal? ");
                    input = Console.ReadLine();
                    if (input == "Equal" || input == "equal" || input == "E")
                    {
                        Console.WriteLine("I have geussed your number!");
                        Console.WriteLine("It took me " + counter + " tries");
                        i = true;
                    }
                    else if (input == "Higher" || input == "higher" || input == "H")
                    {
                        numchange = numchange / 2 + 1;
                        number = number + numchange;
                        counter++;
                        i = false;
                    }
                    else if (input == "Lower" || input == "lower" || input == "L")
                    {
                        numchange = numchange / 2 + 1;
                        number = number - numchange;
                        counter++;

                        i = false;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a vaild answer (H/L) ");
                        i = false;
                    }
                } while (i == false);

            }
            else
            {
                Console.WriteLine("Please enter a vaild answer (Y/N) ");
            }
        }
    }
}