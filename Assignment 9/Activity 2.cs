using System;

public class MyProgram
{
    public static void Main(String[] args)
    {
        int i;
        int value;
        
        value = (int) (UserDataV());
        i = UserDataI();
        LoopKind(i, value);
    }

    private static void ForLoop(int i, int value)
    {
        int repeatvar;
        
        for (repeatvar = 1 ; repeatvar <= i ; repeatvar += 1)
        {
            int sum;
            
            sum = repeatvar * value;
            Console.WriteLine(value.ToString() + " * " + repeatvar + " = " + sum);
        }
    }

    private static void LoopKind(int i, int value)
    {
        String Loop;
        
        Console.WriteLine("Please enter W for while loop, or please enter F for for loop ");
        input(out Loop);
        if (Loop == "W")
        {
            WhileLoop(i, value);
        }
        else
        {
            if (Loop == "F")
            {
                ForLoop(i, value);
            }
            else
            {
                Console.WriteLine("Please enter a valid input");
            }
        }
    }

    private static int UserDataI()
    {
        int i;
        
        Console.WriteLine("Please enter the amount of expressions you would like ");
        input(out i);
        
        return i;
    }

    private static double UserDataV()
    {
        int value;
        
        Console.WriteLine("Please enter the value you would like ");
        input(out value);
        
        return value;
    }

    private static void WhileLoop(int i, int value)
    {
        int returnvar;
        
        returnvar = 1;
        while (returnvar <= i)
        {
            int sum;
            
            sum = returnvar * value;
            Console.WriteLine(value.ToString() + " * " + returnvar + " = " + sum);
            returnvar = returnvar + 1;
        }
    }
    
    // .NET can only read single characters or entire lines from the console.
    // The following functions are designed to help input specific data types.
    
    private static void input(out double result)
    {
        while (!double.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out int result)
    {
        while (!int.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out Boolean result)
    {
        while (!Boolean.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out string result)
    {
        result = Console.ReadLine();
    }
}
