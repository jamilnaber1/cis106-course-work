using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int mileA;
            Console.WriteLine("Please enter your mile amount");
            mileA = Convert.ToInt32(Console.ReadLine());

            int yards = mileA * 1760;
            Console.WriteLine("This is the amount of yards: " + yards);

            int feet = yards * 3;
            Console.WriteLine("This is the amount of feet: " + feet);

            int inchs = feet * 12;
            Console.WriteLine("This is How Many Hours Old You Are: " + inchs);
        }
    }
}
