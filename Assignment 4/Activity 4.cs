using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int Age;
            Console.WriteLine("How Many Years Old Are You ? ");
            Age = Convert.ToInt32(Console.ReadLine());

            int months = Age * 12;
            Console.WriteLine("This is How Many Months Old You Are: " + months);

            int days = Age * 365;
            Console.WriteLine("This is How Many Days Old You Are: " + days);

            int hours = Age * 365 * 24;
            Console.WriteLine("This is How Many Hours Old You Are: " + hours);

            int seconds = Age * 365 * 24 * 60 * 60;
            Console.WriteLine("This is How Many Seconds Old You Are: " + seconds);
        }
    }
}