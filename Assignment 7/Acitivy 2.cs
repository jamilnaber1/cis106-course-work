using System;

public class MyProgram
{
    public static void Main(String[] args)
    {
        double hours;
        double rateperhour;
        double gross;
        
        hours = GetHours();
        rateperhour = GetRateperhour();
        gross = CalculateGross(hours, rateperhour);
        DisplayResult(gross);
    }

    private static double CalculateGross(double hours, double rateperhour)
    {
        double gross;
        double Overtimegross;
        double Overtime;
        
        if (hours > 40)
        {
            Overtime = hours - 40;
            hours = hours - Overtime;
            Overtimegross = Overtime * rateperhour * 1.5;
            gross = hours * rateperhour + Overtimegross;
        }
        else
        {
            gross = hours * rateperhour;
        }
        
        return gross;
    }

    private static void DisplayResult(double gross)
    {
        Console.WriteLine("This Is the Gross Amount Paid in Dollars: ");
        Console.Write(gross);
    }

    private static double GetHours()
    {
        double hours;
        
        Console.WriteLine("How Many Hours Have You Worked");
        input(out hours);
        
        return hours;
    }

    private static double GetRateperhour()
    {
        double rateperhour;
        
        Console.WriteLine("What is your Pay Rate");
        input(out rateperhour);
        
        return rateperhour;
    }
    
    // .NET can only read single characters or entire lines from the console.
    // The following functions are designed to help input specific data types.
    
    private static void input(out double result)
    {
        while (!double.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out int result)
    {
        while (!int.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out Boolean result)
    {
        while (!Boolean.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out string result)
    {
        result = Console.ReadLine();
    }
}
