using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Schema;
using System.Net;

namespace Rextester
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string m_strFilePath = "https://www.w3schools.com/xml/cd_catalog.xml";
            XmlDocument attempt = new XmlDocument();
            bool check;
            check = Vald(m_strFilePath);
            if (check == true)
            {
                attempt.Load(m_strFilePath);
                List<string> title = new List<string>();
                List<string> artist = new List<string>();
                List<string> country = new List<string>();
                List<string> price = new List<string>();
                List<string> year = new List<string>();
                title = Reader(attempt, "TITLE");
                artist = Reader(attempt, "ARTIST");
                country = Reader(attempt, "COUNTRY");
                price = Reader(attempt, "PRICE");
                year = Reader(attempt, "YEAR");
                DisplayResults(title, artist, country, price, year);
                AveragePrice(price);
            }
        }
        private static bool Vald(string m_strFilePath)
        {
            try
            {
                var file = m_strFilePath;
                var settings = new XmlReaderSettings { DtdProcessing = DtdProcessing.Ignore, XmlResolver = null };
                using (var reader1 = XmlReader.Create(new StreamReader(WebRequest.Create(m_strFilePath).GetResponse().GetResponseStream()), settings))
                {
                    var document = new XmlDocument();
                    document.Load(reader1);
                }
                XmlDocument reader2 = new XmlDocument();
                reader2.Load(m_strFilePath);

                if (reader2.ChildNodes.Count == 0)
                {
                    throw new Exception();
                }
                return true;
            }

            catch (Exception exc)
            {
                Console.WriteLine("ERROR: could not find file, or file was empty ");
                return false;
            }
        }

        private static List<string> Reader(XmlDocument attempt, string a)
        {
            XmlNodeList elemList = attempt.GetElementsByTagName(a);
            List<string> returner = new List<string>();
            bool Valdcheck;
            for (int i = 0; i < elemList.Count; i++)
            {
                Valdcheck = ErrorHandle(elemList, a, i);
                if (Valdcheck == true)
                {
                    returner.Add(elemList[i].InnerXml);
                }
                else
                {
                    returner.Add("ERROR HERE");
                }
            }
            return returner;
        }

        private static bool ErrorHandle(XmlNodeList elemList, string a, int i)
        {
            try
            {
                if ( a == "PRICE")
                {
                    bool letters;
                    string numbercheck = elemList[i].InnerXml;
                    letters = numbercheck.All(c => Char.IsNumber(c) || c == '.');
                    if ( letters == false)
                    {
                        Console.WriteLine("There is a non-numeric value loacted here");
                        throw new Exception();
                    }

                }
                return true;
            }
            catch (Exception exc)
            {
                Console.WriteLine("ERROR: Input Error ");
                return false;
            }
        }

        private static void DisplayResults(List<string> title, List<string> artist, List<string> country, List<string> price, List<string> year)
        {
            int amount;
            amount = title.Count();
            for (int i = 0; i < amount; i++)
            {
                Console.WriteLine(title[i] + " - " + artist[i] + " - " + country[i] + " - " + price[i] + " - " + year[i]);
            }
        }

        private static void AveragePrice(List<string> price)
        {
            double average;
            List<double> result = price.Select(x => double.Parse(x)).ToList();
            average = result.Sum() / result.Count;
            string averagefinal = average.ToString("C");
            Console.WriteLine("\n" + result.Count + " items - " + averagefinal + " average price\n");
        }
    }
}