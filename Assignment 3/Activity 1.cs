using System;

public class MyProgram
{
    public static void Main(String[] args)
    {
        // I have pushed this comment form the IDE
        // The purpose of this program is to show order of operations
        // I picked 3 diffrent variables( a, b, c) and made equations that express the order of operations 
        // I also made it user friendly by making prompts so the user know what they are entering the values for
        int a;
        int b;
        int c;
        
        Console.WriteLine("Please enter an integer for a");
        input(out a);
        Console.WriteLine("Please enter an integer for b");
        input(out b);
        Console.WriteLine("Please enter an integer for c");
        input(out c);
        Console.WriteLine("Your answer for b+a*c ");
        Console.WriteLine(b + a * c);
        Console.WriteLine("Your answer for (b+a)*c ");
        Console.WriteLine((b + a) * c);
        Console.WriteLine("Your answer for a+(b^2 *c)");
        Console.WriteLine(a + Math.Pow(b, 2) * c);
    }
    
    // .NET can only read single characters or entire lines from the console.
    // The following functions are designed to help input specific data types.
    
    private static void input(out double result)
    {
        while (!double.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out int result)
    {
        while (!int.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out Boolean result)
    {
        while (!Boolean.TryParse(Console.ReadLine(), out result));
    }
    
    private static void input(out string result)
    {
        result = Console.ReadLine();
    }
}
