Jamil Naber

Why am i taking this course?
I want to take this course because I want to be a computer science major in the future.
With that in mind, I�m taking CIS because knowing computer logic is important when doing coding. 
I also need to keep me in the loop with computer class since this will be my only one this semester.

What programming language did I pick?
C#

Why?
I chosen c# because I also learned c++ in CSC 121/CSC 122 which would make it easier to translate information from both classes.
Also I like modding for video games. Also c#, although in decline, is very useful for developers to learn since both C and C# inherent characteristics from C++. 
Also I don't plan on only learning c#, I have future plans.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
What I learned from the IDE and programming?
This week I have learned a lot in CIS106 with making my program hello.cs to say my name.
Also I learned how to push my code from an IDE to my bitbucket with making no mistakes. 
I still like using a source coder, like neditn, and a compiler, like gnome, because I like to see and do the whole process by myself. 
Maybe in the future I will like IDE more, but right now I will stick to my set up.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
I learned about the usage of inputs, outputs, and variable. For my activity, I pick how to demonstrate the order of operations with a program.
I use Flowgroithm as an outline and turn in the assignments associated with it, but I also did something else.
I wrote the code on my own then compared it to the one generated by Flowgroithm and found out Flowgroithm adds a lot of unnecessary lines that don't apply to my code.
I'm happy to show anyone who asked for the comparison. With the variables available, (Char, Bool, Double, int), we are able to use a blank space to assign a value.
We have outputs of which can be prompts, values of data, and/or answer to equations. This also has an application to life with mathematics because we use variables all the time.
We define them, and better yet we can solve some unknown variables Since I want to become a computer scientist, Learning variables is the foundation of my practice.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
As this is the same question from last week, I will be talking about the three actives we had to complete this week. 
The three actives I picked were gross pay, Lengths, and age. 
With all of these I found that they all required to do some kind of expression of mathematics which is huge in my field of work. 
As a computer scientist, I'm required to have a deep understanding of math, hence me taking differential equations and calculus 3. 
With some problems you are forced to make your own formula/equation to solve a programming problem.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
This week we learned something new about the structure that programs use, so it can become more complex without lots of confusion. 
The usage of functions is what the topic of this week was. 
As we used the same activities form last week, we see the major differences coding with functions opposed to just writing out some random code. 
This does make it seem longer for now, but in the future we will need this in order to stay organized with all of our complex programs. 
In my future job, this will become a religious practice because I will be coding very complex programs.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
With the continued usage of functions, I learned how to use them without using flogorithm.
Instead, I used my IDE to have the code be made by my own scratch ideas.
What I find interesting is that with everyone in this class will have a solution to the problem, but a different execution.
In my future job field, that of being a programmer, I need to be able to use functions as for they are needed to be organized.
Also, I like the pace of this class because it's kinda like a real world environment of making your own time available to finish projects.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
This week we continued on the idea of using function, with a twist included.
We have a new addition to our arsenal with condition codes.
This helps us direct code that needs more attention, and that needs to be done differently than the rest.
This helps, because now we don't need to make a whole new file for just one problem, we can now solve multiple problems in the same file.
The usage of this in the future is, once again, a must for me as programmer can't just write a code that pleases one problem.
We need to be able to write code that solves a range of problems to be time efficient.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
We continued our newly found knowledge of using conditions in code.
With the number of activities we had done this week, we can see how useful this is to our codes.
Without these conditions, we would need more lines of codes to ask the user what they want the program to do.
The only problem I'm having this week does not involve conditions, but just the different kinds of functions I need to use to avoid a logical error.
As I said last week, my future job will need this because I will become a programmer.
I Don't think I need to go in more detail for why a programmer might need this.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Session 9 brought another key component of coding called loops.
These loops help us repeat section/s of the code when needed.
They are set to have a limited amount of times they will run which is provided by either the programmer or the user.
This helps us, because we don't have to keep on retyping a part of a code, and makes it easier to see what you are doing.
The usage of this new tool can relate to my work field as a programmer. 
It is a necessity as a programer to use loops for efficiency and space.
For example, If we need some kind of program that needs to take averages we can use loops.
Everytime the user enters a number it is stored until the loopit is ran again.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
In Session 10, we continue to learn the applications of Loops in codes.
With the range of different activities this week, we find ourselves looking for the most efficient way to make a code work properly.
Loops help us because they are about to shorten down code and make it easier to follow the logic of the code.
Without the help of loops, codes would be harder to follow and much longer than necessary.
The real life applications for me would have to be writing programs.
Instead of me just write the same piece of code manually every single time, I can use one loop that will continue a part of code the amount of times I set it too.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
In session 11, we were introduced to a new type of information storage called arrays.
Arrays can be best describe to be a continuous variable that can store multiple numbers, and that can be called at any time.
First, we have to name the array whatever we want, like a variable.
Then, the name is followed by how many data point holders you want.
This holder can be defined by a number or another variable that contains whole numbers.
Arrays are most helpful with loops, because we can call back to the users original input after using those numbers were in our calculations.
In my example with the score input, instead of having one big variable that totaled up all the scores, we can hold all the independent scores and add them up later.
This means we can use those number for something else if we wish without the risk of losing them.
In my future working field as a programmer, this will be, once again, another important tool.
I need to be able to storage all the information that the user gives me and process it, but I do not want to lose that raw input while processing.
This will make my life easier, and my users life easier.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Session 12 was a continuation of the usage of arrays.
We needed to use arrays in three different codes this week which allowed us to have extra practice with arrays.
In activity 2, I find the arrays as mainly a place to store strings that can be found later on. 
This allows us to not to make independent if else statements to find these string; rather, we find the numeric value with a parallel strings.
The benefit is found when making larger coded that would need less code to process meaning less time, which in the real world translate to more money.
Activity 3 usage of arrays were not the issue that I was having, more that I was having issues with making the equation work. 
In my real world job, arrays will need to be used a lot for this is the most efficient way to store user data.
Without this, we would be forced to use our time to typing in repetitive code that really does not have any substance.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
In session 13, we are learning about file storage and usage, with the focus being on strings this week.
I worked on activity 1, which was seemingly easy to do.
We needed to just set 3 to 4 different functions to do what it was asking, a get name, a change first name, and a change last name.
The value type of string have built-in properties in C#, like in many other major languages, which simplifies the task a whole lot.
The benefit of learning the properties of strings to to help with getting user data, and also changing the value to the output we need.
In my future job of being a programmer, I will need to be comfortable with the value type of strings.
The strings will help me with data, and in a more advanced setting, calling for different files to be found.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Session 14 was a continuation of session 13 with a new element put into place.
The new element we see this week is with files with reading, finding and writing.
With the information we have about how to use strings, we can now use that to help us with files.
The main component that involves these two is grabbing information.
What we learned last week with what we learned this week, we can grab data from files and make outputs with new information.
In my future career as a computer programmer, this will be a must as I will be working with mupilt files when coding.
Learning how to find, create and edit files from a coding point-of-view is helpful to keep information organized, which makes me organized. 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
In Session 16, we had worked on our final project for this class.
This project works on the ideas that we had learned in the overall semester with the usage of functions, string manipulation, and more.
What I have learned overall in the programing aspects of this class, is that if you understand the foundation of one subject, the next  one is easy.
When saying this I meaning that once you learn one aspect of coding, the next is just a build off of that.
The online aspect helps this because everything is organized, and has set dates when everything is due.
The time I had to look outside of the book showed me that even with the given text, programming is constantly changing.
My real world application of this class is a huge help with it�s learning style.
As a programmer, I need to be on my top game when it comes to the knowledge of new coding ideas.
This class also prepared the foundation of learning any other language I want without the assistance of an instructor.  
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------